from fastapi.testclient import TestClient

from app import main

client = TestClient(main.app)


def test_random_joke_response_code_main():
    response = client.get("/random")
    assert response.status_code == 200


def test_get_joke_by_id_not_found():
    response = client.get("/0")
    assert response.status_code == 400
    assert response.json() == {"detail": "Id must be greater or equal than 1"}


def test_get_joke_by_id_bad_id():
    response = client.get("/12")
    assert response.status_code == 404
    assert response.json() == {"detail": "Joke not found"}
