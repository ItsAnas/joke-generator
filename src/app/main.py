from fastapi import FastAPI
from app.services import joke

app = FastAPI()

app.include_router(joke.router)

@app.get("/")
async def root():
    return {"message": "Hello World"}