from fastapi import APIRouter
from fastapi import status, HTTPException, status
import random

from app.data import joke_db

router = APIRouter()


@router.get("/random")
async def get_random_joke():
    return random.choice(joke_db.db)


@router.get("/{id}")
async def get_joke_by_id(id: int):
    if id < 1:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Id must be greater or equal than 1",
        )
    if id > len(joke_db.db):
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Joke not found"
        )
    final_id = id - 1
    joke = joke_db.db[final_id]
    return joke